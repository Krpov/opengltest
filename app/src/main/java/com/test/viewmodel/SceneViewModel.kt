package com.test.viewmodel

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.opengl.Square
import com.test.scene.Ray2D
import com.test.scene.SceneObject
import com.test.scene.SceneTimer
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

class SquareViewModel : ViewModel(), SceneObject {

    private var startRay: Ray2D? = null
    private var currentRay: Ray2D? = null
    private val timer = SceneTimer()
    private var square: Square? = null
    private var currentPosition = Vector2D(0.0, 0.0)

    private val updateContiniously = MutableLiveData<Boolean>().apply {
        value = false
    }

    fun getUpdateContiniously(): LiveData<Boolean> = updateContiniously

    override fun onSurfaceCreated(resources: Resources) {
    }

    override fun onDrawFrame(
        mvpMatrix: FloatArray
    ) {

        val dt = timer.update()
        currentRay?.let { ray ->
            ray.nextRay?.start?.let { nextRayStart ->
                if ((nextRayStart.x < 0 && currentPosition.x <= nextRayStart.x) ||
                    (nextRayStart.x > 0 && currentPosition.x >= nextRayStart.x)
                ) {
                    currentPosition = nextRayStart
                    currentRay = ray.nextRay
                }
            }
        }

        currentRay?.direction?.let {
            currentPosition = currentPosition.add(
                it.scalarMultiply(dt * SPEED)
            )
        }

        square?.setPosition(currentPosition)
    }

    override fun onSurfaceChanged(viewportProjection: List<Vector2D>) {
        val squareWidth = square?.width?.toDouble() ?: 0.0
        val offsets = listOf(
            Vector2D(squareWidth, squareWidth),
            Vector2D(-squareWidth, squareWidth),
            Vector2D(squareWidth, -squareWidth),
            Vector2D(-squareWidth, -squareWidth)
        )

        viewportProjection.reversed().forEachIndexed { index, vector2D ->
            val startPoint = vector2D.add(offsets.get(index))
            startRay = Ray2D(
                startPoint, startRay?.start?.subtract(startPoint)?.normalize(), startRay
            )
        }

        if (currentRay == null) {
            startRay?.let {
                currentRay = it
                currentPosition = it.start
            }
        }
    }

    override fun onRelease() {
        square = null
    }

    fun setSquare(s: Square) {
        square = s
    }

    fun onStartClick() {
        updateContiniously.value = true
        timer.start()
    }

    fun onStopClick() {
        updateContiniously.value = false
        timer.stop()
    }

    companion object {
        private const val TAG = "SquareViewModel"
        private const val SPEED = 30.0f
    }
}