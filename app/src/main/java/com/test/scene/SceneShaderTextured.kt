package com.test.scene

import android.opengl.GLES20
import java.nio.FloatBuffer

class SceneShaderTextured : SceneShader() {
    override val vertexSrc =
        "uniform mat4 uMVPMatrix;\n" +
            "attribute vec2 aPosition;\n" +
            "attribute vec2 aTexPos;\n" +
            "varying vec2 vTexPos;\n" +
            "void main() {\n" +
            "  vTexPos = aTexPos;\n" +
            "  gl_Position = uMVPMatrix * vec4(aPosition.xy, 0.0, 1.0);\n" +
            "}"

    override val fragmentSrc =
        "precision mediump float;\n" +
            "uniform sampler2D uTexture;\n" +
            "varying vec2 vTexPos;\n" +
            "void main(void)\n" +
            "{\n" +
            "  gl_FragColor = texture2D(uTexture, vTexPos);\n" +
            "}"

    override fun setVertexBuffer(buffer: FloatBuffer) {
        buffer.position(POSITION_OFFSET)
        GLES20.glVertexAttribPointer(
            aPosition,
            POSITION_SIZE,
            GLES20.GL_FLOAT,
            false,
            (POSITION_SIZE + TEXTURE_SIZE) * FLOAT_SIZE,
            buffer
        )
        GLES20.glEnableVertexAttribArray(aPosition)
        buffer.position(TEXTURE_OFFSET)
        GLES20.glVertexAttribPointer(
            aTexPos,
            TEXTURE_SIZE,
            GLES20.GL_FLOAT,
            false,
            (POSITION_SIZE + TEXTURE_SIZE) * FLOAT_SIZE,
            buffer
        )
        GLES20.glEnableVertexAttribArray(aTexPos)
    }
}