package com.example.android.opengl

import android.content.res.Resources
import android.opengl.Matrix
import com.test.scene.SceneBuffer
import com.test.scene.SceneObject
import com.test.scene.SceneShader
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

class Line(
    private val shader: SceneShader,
    private val sideSize: Float = 10.0f
) : SceneObject {

    private val positionMatrix = FloatArray(16)
    val sceneBuffer = SceneBuffer()

    init {
        Matrix.setIdentityM(positionMatrix, 0)
    }

    override fun onSurfaceCreated(resources: Resources) {
    }

    override fun onSurfaceChanged(viewportProjection: List<Vector2D>) {
        val squareWidth = sideSize.toDouble()
        val line = listOf(
            Vector2D(-squareWidth, -squareWidth).add(viewportProjection[0]),
            Vector2D(squareWidth, -squareWidth).add(viewportProjection[1]),
            Vector2D(-squareWidth, squareWidth).add(viewportProjection[2]),
            Vector2D(squareWidth, squareWidth).add(viewportProjection[3])
        )

        sceneBuffer.setVertextBuffer(
            floatArrayOf(
                line[0].x.toFloat(), line[0].y.toFloat(),
                line[1].x.toFloat(), line[1].y.toFloat(),
                line[2].x.toFloat(), line[2].y.toFloat(),
                line[3].x.toFloat(), line[3].y.toFloat()
            )
        )
        sceneBuffer.setIndexBuffer(
            shortArrayOf(0, 1, 2, 1, 2, 3)
        )
    }

    override fun onDrawFrame(mvpMatrix: FloatArray) {
        val scratch = FloatArray(16)
        Matrix.multiplyMM(scratch, 0, mvpMatrix, 0, positionMatrix, 0)
        shader.preRender()
        shader.setMVPMatrix(scratch)
        shader.setVertexBuffer(sceneBuffer.vertexBuffer)
        sceneBuffer.drawLine()
    }

    override fun onRelease() {
        sceneBuffer.releaseBuffer()
    }
}