package com.example.android.opengl

import android.content.res.Resources
import android.opengl.Matrix
import com.test.scene.SceneBuffer
import com.test.scene.SceneObject
import com.test.scene.SceneShader
import com.test.scene.SceneTexture
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

class Square(
    private val shader: SceneShader,
    private val texture: SceneTexture,
    private val sideSize: Float = 10f
) : SceneObject {

    private val positionMatrix = FloatArray(16)
    val sceneBuffer = SceneBuffer()

    val width = sideSize

    init {
        Matrix.setIdentityM(positionMatrix, 0)
    }

    override fun onSurfaceCreated(resources: Resources) {
        sceneBuffer.setVertextBuffer(
            floatArrayOf(
                -sideSize, sideSize,
                0f, 0f,
                -sideSize, -sideSize,
                0f, 1f,
                sideSize, -sideSize,
                1f, 0f,
                sideSize, sideSize,
                1f, 1f
            )
        )
        sceneBuffer.setIndexBuffer(
            shortArrayOf(0, 1, 2, 0, 2, 3)
        )
    }

    override fun onSurfaceChanged(viewportProjection: List<Vector2D>) {
    }

    override fun onDrawFrame(mvpMatrix: FloatArray) {
        val scratch = FloatArray(16)
        Matrix.multiplyMM(scratch, 0, mvpMatrix, 0, positionMatrix, 0)
        texture.activateTexture()
        shader.preRender()
        shader.setMVPMatrix(scratch)
        shader.setTexture()
        shader.setVertexBuffer(sceneBuffer.vertexBuffer)
        sceneBuffer.draw()
    }

    fun setPosition(position: Vector2D) {
        Matrix.setIdentityM(positionMatrix, 0)
        Matrix.translateM(positionMatrix, 0, position.x.toFloat(), position.y.toFloat(), 0f)
    }

    override fun onRelease() {
        sceneBuffer.releaseBuffer()
    }
}