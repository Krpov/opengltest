package com.test.scene

import android.content.res.Resources
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D

interface SceneObject {
    fun onSurfaceCreated(resources: Resources)
    fun onSurfaceChanged(viewportProjection: List<Vector2D>)
    fun onDrawFrame(mvpMatrix: FloatArray)
    fun onRelease()
}