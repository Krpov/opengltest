package com.test.view

import android.content.res.Configuration
import android.opengl.GLSurfaceView
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.test.R
import com.test.viewmodel.SquareViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val viewModel by viewModels<SquareViewModel>()

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        start_button.setOnClickListener {
            viewModel.onStartClick()
        }

        stop_button.setOnClickListener {
            viewModel.onStopClick()
        }

        viewModel.getUpdateContiniously().observe(this, Observer {
            if (it) {
                surface_view.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY)
            } else {
                surface_view.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY)
            }
        })

    }

    override fun onPause() {
        super.onPause()
        surface_view.onPause()
    }

    override fun onResume() {
        super.onResume()
        surface_view.onResume()
    }

    override fun onBackPressed() {
        surface_view.releaseRenderer()
        super.onBackPressed()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        surface_view.releaseRenderer()
        super.onConfigurationChanged(newConfig)
    }
}